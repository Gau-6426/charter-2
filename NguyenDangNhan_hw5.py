#Part1
def print_volume(r):
    from math import pi
    v = (4/3)*pi*(r**3)
    print(v)

print_volume(11) 
#5575.279762570686
print_volume(20)
#33510.32163829113
print_volume(4)
#268.082573106329   

#Part2
# Rectangular
def Rectangular (a,b):
    if a != b:
        s = a * b
        p = (a + b) *2 
        print(s,p)
    elif a < b:
        print("Error")    
    else:
        print("Error")

Rectangular(6,2) 
#12 16
Rectangular(80,52)
#4160 264
Rectangular(20,10)
#200 60

#sin, cos, tan
def math(x):
    from math import sin, cos, tan
    s = sin(x)
    c = cos(x)
    t = tan(x)
    print(s,c,t)
    
math(20)
# 0.9129452507276277 0.40808206181339196 2.237160944224742
math(78)
# 0.5139784559875352 -0.8578030932449878 -0.5991799983411151
math(49)
#-0.9537526527594719 0.3005925437436371 -3.172908552159191