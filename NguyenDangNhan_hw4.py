def new_line():
    print('.')
def three_lines():
    print('=_=')
    print('=_=')
    print('=_=')
    

def nine_lines():
    three_lines()
    three_lines()
    three_lines()
    
        
def clear_screen():
    nine_lines()
    nine_lines()
    three_lines()
    three_lines()
    new_line()
    
message1 = '9 lines successfully printed'
message2 = 'Calling clearScreen() to print 25 lines'
message3 = "25 lines successfully printed"

def main():   
    print(nine_lines(), message1)
    print(message2)
    clear_screen()
    print(message3)
if __name__ == '__main__':
    main()
    


