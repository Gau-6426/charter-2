#Define a function that takes an argument. Call the function. Identify what code is the argument and 
# what code is the parameter.
def Example1(x): 
  print(x)

# khi gáng x  là giá trị bất kì nó sẽ print ra giá trị đó hoặc thực hiện phép tính đó
Example1(9-1)
Example1("hello")
Example1(1)

# Call your function from Example 1 three times with different kinds of arguments: a value, a variable,
# and an expression. Identify which kind of argument is which. 

def Example2():
    x = 2021
# Example2() nếu chạy chương trình 
# print(x)  nó sẽ trả về lỗi, bởi vì nó là một biến cục bộ
# bởi vì x chưa được định nghĩa

print(Example2(x))



# Create a function with a local variable. Show what happens when you try to use that variable outside 
# the function. Explain the results.
def Example3(f3):
    print('really unical')


print(Example3(2021))
#print(f3) Nếu chạy đoạn code này thì sẽ báo lỗi NameError


# Create a function that takes an argument. Give the function parameter a unique name. Show what happens 
# when you try to use that parameter name outside the function. Explain the results.
def Example4(f4):
    print(f4)
# variable bên trong một hàm và một biến có cùng tên bên ngoài 
# trong số đó là hai biến khác nhau 
# và các giá trị của chúng không trùng lặp hoặc thay thế
Example4(f4)


# Show what happens when a variable defined outside a function has the same name as a local variable inside
# a function. Explain what happens to the value of each variable as the program runs.
def my_func():
    var = 20  # inner variable
    return var

var = 10  # outer variable
print(var)
print(my_func())
print(var)
#Mọi thay đổi của biến bên trong var sẽ không ảnh hưởng đến biến bên ngoài var và ngược lại.